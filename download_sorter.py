import os, shutil

# Setup
home = os.getenv("HOME")
video = os.path.join(home, "Videos")
picture = os.path.join(home,"Pictures")

# Move to Downloads directory
download = os.path.join(home, "Downloads")
os.chdir(download)

def sort_downloads(path):

    # List files
    files = os.listdir(path)
    for file in files:
        # Checks for images
        if file.endswith(".jpg") or file.endswith(".png"):
            shutil.move(file, picture)
        # Checks for videos
        elif file.endswith(".mp4") or file.endswith(".webm"):
            shutil.move(file, video)
        else:
            # Prints unhandled files
            print(f"{file} unable to be sorted.")

sort_downloads(download)

